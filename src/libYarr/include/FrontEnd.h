#ifndef FRONTEND_H
#define FRONTEND_H

// #################################
// # Author: Timon Heim
// # Email: timon.heim at cern.ch
// # Project: Yarr
// # Description: Abstract FE class
// # Comment: Combined multiple FE 
// ################################

#include <string>
#include <utility>

#include "ClipBoard.h"
#include "HistogramBase.h"
#include "EventDataBase.h"
#include "HwController.h"
#include "FrontEndGeometry.h"

#include "storage.hpp"

// Status enum
enum yarrStatus {
    yarrSuccess = 0,
    yarrFailure = -1,
};

//! extra int trigger tags to pass more feedback from the data processors
#define PROCESSING_FEEDBACK_TRIGGER_TAG_ERROR  -10
#define PROCESSING_FEEDBACK_TRIGGER_TAG_RR      -2
#define PROCESSING_FEEDBACK_TRIGGER_TAG_Control -3  //!< HPRs in Strips
#define PROCESSING_FEEDBACK_UNDEFINED_BCID -1

//! \brief RawData processing information from data processors
typedef struct FeedbackProcessingInfo
{
    unsigned packet_size = 0; //!< the size of the packet that the FE sent, i.e. RawData.getSize()
    int trigger_tag = PROCESSING_FEEDBACK_TRIGGER_TAG_ERROR; //!< l0id of the triggered data packets, and extra negative tags for RR etc
    int bcid = PROCESSING_FEEDBACK_UNDEFINED_BCID;
    unsigned n_clusters = 0;  //!< n_clusters in Strips & the number of hits in Pixels?
} FeedbackProcessingInfo;

class Bookkeeper;
class FrontEndConnectivity;

class FrontEnd {
    public:
        FrontEnd() = default;
        virtual ~FrontEnd() = default;
        
        virtual void init(HwController *arg_core, const FrontEndConnectivity& fe_cfg)=0;

        bool getActive() const;
		bool isActive() const;
		void setActive(bool active);
        virtual void makeGlobal(){};
        virtual std::unique_ptr<FrontEnd> getGlobal() {return nullptr;}
        virtual void connectBookkeeper(Bookkeeper* k){};
       
        virtual void configure()=0;
        virtual yarrStatus checkCom() {return yarrSuccess;}
        virtual yarrStatus hasValidName() { return yarrSuccess; }

        // A parallel reset that undos any configuration
        virtual void resetAllHard() {}
        // A parallel reset that keeps configuration but reset counters/datapath
        virtual void resetAllSoft() {}

        // Set/Get Register in memory only
        virtual yarrStatus setNamedRegister(std::string name, const uint16_t value) {return yarrFailure;};
        virtual yarrStatus getNamedRegister(std::string name, uint16_t &value) {return yarrFailure;};
    
        // Write register to memory and chip
        virtual yarrStatus writeNamedRegister(std::string name, const uint16_t value) = 0;
        // Read register from chip to memory and return value through reference
        virtual yarrStatus readNamedRegister(std::string name, uint16_t &value) {return yarrFailure;};
        // Read register from chip to memory, write register to memory, and then to chip
        virtual yarrStatus readUpdateWriteNamedRegister(std::string name, const uint16_t value) {return yarrFailure;};

        /// Configures ADC
        virtual void confAdc(uint16_t MONMUX, bool doCur) {}

        virtual void setInjCharge(double, bool, bool) = 0;

        // Clipboards to buffer data
        ClipBoard<RawDataContainer> clipRawData;
        ClipBoard<EventDataBase> clipData;
        ClipBoard<HistogramBase> clipHisto;
        ClipBoard<FeedbackProcessingInfo> clipProcFeedback;
        std::vector<std::unique_ptr<ClipBoard<HistogramBase>> > clipResult;
        
        FrontEndGeometry geo;

    protected:
        bool active;
        RxCore *m_rxcore;
};

class FrontEndConnectivity {
    public:
        FrontEndConnectivity() {
            initFeConnectivity(99, 99);
        }

        FrontEndConnectivity(unsigned arg_channel) {
            initFeConnectivity(arg_channel);
        }

        FrontEndConnectivity(unsigned arg_txChannel, unsigned arg_rxChannel) {
            initFeConnectivity(arg_txChannel, arg_rxChannel);
        }

        FrontEndConnectivity(const FrontEndConnectivity& cfg) {
            initFeConnectivity(cfg.getTxChannel(), cfg.getRxChannel());
        }

        virtual void initFeConnectivity(const FrontEndConnectivity& cfg) {
            initFeConnectivity(cfg.getTxChannel(), cfg.getRxChannel());
        }

	virtual void initFeConnectivity(unsigned arg_txChannel, unsigned arg_rxChannel){
	    txChannel = arg_txChannel;
	    rxChannel = arg_rxChannel;
	    lockCfg = false;
	}

	virtual void initFeConnectivity(unsigned arg_channel){
	    initFeConnectivity(arg_channel, arg_channel);
	}

	virtual ~FrontEndConnectivity()= default;

        unsigned getChannel() const {return rxChannel;}
		unsigned getTxChannel() const {return txChannel;}
		unsigned getRxChannel() const {return rxChannel;}

        void setChannel(unsigned channel) {txChannel = channel; rxChannel = channel;}
		void setChannel(unsigned arg_txChannel, unsigned arg_rxChannel) {txChannel = arg_txChannel; rxChannel = arg_rxChannel;}
        void setChannel(const FrontEndConnectivity &cfg) {setChannel(cfg.getTxChannel(), cfg.getRxChannel());}

        bool isLocked() const {return lockCfg;}
        void setLocked(bool v) {lockCfg = v;}

    protected:
        unsigned txChannel;
        unsigned rxChannel;
        bool lockCfg;

};

class FrontEndCfg : public FrontEndConnectivity {
    public:
        FrontEndCfg() : FrontEndConnectivity() {
            name = "JohnDoe";
            enforceChipIdInName = false;
        }

	    FrontEndCfg(FrontEndCfg& cfg) : FrontEndConnectivity(cfg) {
	        name = cfg.getName();
	        enforceChipIdInName = cfg.checkChipIdInName();
	    }

        virtual ~FrontEndCfg()= default;
        
        virtual double toCharge(double)=0;
        virtual double toCharge(double, bool, bool)=0;
        virtual void writeConfig(json &) =0;
        virtual void loadConfig(const json &)=0;

        virtual unsigned getPixelEn(unsigned col, unsigned row) = 0;
        // col/row starting at 0,0
        virtual void maskPixel(unsigned col, unsigned row) = 0;
        /// Enable (disable mask) for all pixels
        virtual void enableAll() = 0;

        virtual std::tuple<json, std::vector<json>> getPreset(const std::string& systemType="SingleChip");

        std::string getName() {return name;}
        bool checkChipIdInName() { return enforceChipIdInName; }

        // Returns converted ADC counts (float) and unit (string)
	    virtual std::pair<float, std::string> convertAdc(uint16_t ADC, bool meas_curr) {return std::make_pair(0.0, "None");} 
        
        void setName(std::string arg_name) {name = std::move(arg_name);}
    
    protected:
        std::string name;
        bool enforceChipIdInName;
};

#endif
